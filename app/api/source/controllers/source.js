'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  findVotedSource(ctx) {
    return strapi.services.source.findVotedSource(ctx.query);
  },
  countVotedSource(ctx) {
    return strapi.services.source.countVotedSource(ctx.query);
  },
  getVotedSource(ctx) {
    const { id } = ctx.params;
    return strapi.services.source.getVotedSource(id);
  },
  getUtilizedTags(ctx) {
    return strapi.services.source.getUtilizedTags();
  },
};
