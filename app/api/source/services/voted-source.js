const { Types } = require('mongoose');
const { convertRestQueryParams } = require('strapi-utils');

function getCategories(params) {
  if (params.tags_category) {
    const category = params.tags_category;
    return [].concat(category);
  }
}

function getTags(params) {
  if (params.tags) {
    const tags = params.tags;
    return [].concat(tags);
  }
}

function sortNum(field) {
  if (field.order === 'asc') {
    return -1;
  } else {
    return 1;
  }
}

function constructSortObject(queryParams) {
  const sort = { $sort: {} };
  if (queryParams.sort) {
    queryParams.sort.forEach((sortItem) => {
      if (sortItem.field === 'featured') {
        sort.$sort[ sortItem.field ] = sortNum(sortItem);
      } else {
        sort.$sort[ sortItem.field ] = sortNum(sortItem);
      }
    });
  } else {
    sort.$sort.featured = -1;
    sort.$sort.votesAll = -1;
    sort.$sort.votesLastWeek = -1;
    sort.$sort.votesLastMonth = -1;
    sort.$sort.updatedAt = -1;
  }
  return sort;
}

function getToday() {
  const today = new Date();
  today.setHours(0, 0, 0, 0);
  return today;
}

function getWeekAgo() {
  const weekAgo = new Date();
  weekAgo.setHours(0, 0, 0, 0);
  weekAgo.setDate(weekAgo.getDate() - 7);
  return weekAgo;
}

function getMonthAgo() {
  var monthAgo = new Date();
  monthAgo.setHours(0, 0, 0, 0);
  monthAgo.setMonth(monthAgo.getMonth() - 1);
  return monthAgo;
}

function sourceMatchByTags(params) {
  const categories = getCategories(params);
  const tags = getTags(params);

  const filter = {};
  if (categories) {
    filter.$and = [ { tags: { $all: categories.filter(Types.ObjectId.isValid).map(Types.ObjectId) } } ];
  }

  if (tags) {
    filter.tags = {
      $in: tags.filter(Types.ObjectId.isValid).map(Types.ObjectId)
    };
  }

  return {
    $match: filter
  };
}

function sourceMatchById(id) {
  return {
    $match: { _id: Types.ObjectId(id) }
  };
}

async function votedSourceAggPipeline(match, start = 0, limit = 1, sort) {

  const featuredTag = await strapi.models[ 'tag' ].findOne({ text: 'Featured' });

  const today = getToday();
  const weekAgo = getWeekAgo();
  const monthAgo = getMonthAgo();

  if (!sort) {
    sort = {
      $sort: { _id: 1 }
    };
  }

  return strapi.models[ 'source' ].aggregate([
    match,
    {
      $lookup: {
        from: 'votes',
        localField: '_id',
        foreignField: 'source',
        as: 'votes'
      }
    },
    {
      $addFields: {
        featured: featuredTag ? { $in: [ featuredTag._id, '$tags' ] } : false,
        votesLastMonth: {
          $size: {
            $filter: {
              input: '$votes',
              as: 'vote',
              cond: { $gte: [ '$$vote.createdAt', monthAgo ] }
            }
          }
        },
        votesLastWeek: {
          $size: {
            $filter: {
              input: '$votes',
              as: 'vote',
              cond: { $gte: [ '$$vote.createdAt', weekAgo ] }
            }
          }
        },
        votesToday: {
          $size: {
            $filter: {
              input: '$votes',
              as: 'vote',
              cond: { $gte: [ '$$vote.createdAt', today ] }
            }
          }
        },
        votesAll: { $size: '$votes' }
      }
    },
    { $project: { votes: false } },
    sort,
    { $skip: start },
    { $limit: limit }
  ]);
}

async function utilizedTagsAggPipeline() {
  const tags = await strapi.models['tag'].find();
  const sources = await strapi.models['source'].find();
  let tagsCount = tags.map((simpleTag) => {
    return { _id: simpleTag._id, count: 0, text: simpleTag.text, id: simpleTag.id, type: simpleTag.type };
  });

  sources.forEach((source) => {
    source.tags.forEach((sourceTag) => {
      tagsCount.find((tag) => {
        return String(tag._id) === String(sourceTag);
      }).count++;
    })
  })

  return tagsCount.filter(tag => tag.count > 0);
}

module.exports = {
  async findVotedSource(params) {
    const queryParams = convertRestQueryParams(params);
    const sort = constructSortObject(queryParams);
    return votedSourceAggPipeline(sourceMatchByTags(params), queryParams.start, queryParams.limit, sort);
  },
  async countVotedSource(params) {
    const [ result ] = await strapi.models[ 'source' ].aggregate([
      sourceMatchByTags(params),
      { $count: 'count' }
    ]);
    if (!result) {
      return 0;
    }
    return result.count;
  },
  async getVotedSource(id) {
    if (!Types.ObjectId.isValid(id)) {
      return;
    }
    const [ result ] = await votedSourceAggPipeline(sourceMatchById(id));
    return result;
  },
  async getUtilizedTags() {
    const result = await utilizedTagsAggPipeline();
    return result;
  }
};
