'use strict';

const { buildQuery, convertRestQueryParams } = require('strapi-utils');
const { countVotedSource, findVotedSource, getVotedSource, getUtilizedTags } = require('./voted-source');


function getCategories(params) {
  if (params.tags_category) {
    const category = params.tags_category;
    delete params.tags_category;
    return [].concat(category);
  }
}

function findQuery(params, populate) {
  const categories = getCategories(params);

  const filters = convertRestQueryParams(params);

  const query = buildQuery({
    model: strapi.models[ 'source' ],
    filters,
    populate: populate
  });

  if (categories) {
    query.and(categories.map((category) => {
      return { tags: category };
    }));
  }

  return query;
}


module.exports = {
  find(params, populate) {
    const query = findQuery(params, populate);
    return query.exec();
  },
  count(params) {
    const query = findQuery(params);
    return query
      .count()
      .exec();
  },
  findVotedSource,
  countVotedSource,
  getVotedSource,
  getUtilizedTags
};
