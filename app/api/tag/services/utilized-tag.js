'use strict';

module.exports = {
  findUtilizedTags() {
    return strapi.models[ 'tag' ].aggregate([
      { $match: { type: { $ne: 'system' } } },
      {
        $lookup: {
          from: 'sources',
          localField: '_id',
          foreignField: 'tags',
          as: 'sources'
        }
      },
      {
        $addFields: {
          sources: { $size: '$sources' }
        }
      },
      { $match: { sources: { $gt: 0 } } }
    ]);
  }
};
